import {format} from '../utils/format.js'

/**
 * @property a {number}
 * @property b {number}
 */
export class Vector {
    static NULL = Vector.null()

    /**
     * @return {Vector}
     */
    static null() {
        return new Vector(0, 0);
    }

    /**
     * @param a {number}
     * @param b {number}
     */
    constructor(a, b) {
        this.a = a
        this.b = b
    }

    /**
     * @param that {Vector}
     * @return {Vector}
     */
    add(that) {
        return new Vector(this.a + that.a, this.b + that.b);
    }

    /**
     * @param that {Vector}
     * @return {Vector}
     */
    sub(that) {
        return new Vector(this.a - that.a, this.b - that.b)
    }

    /**
     * @param scalar {number}
     * @return {Vector}
     */
    muls(scalar) {
        return new Vector(this.a * scalar, this.b * scalar)
    }

    /**
     * Hadamard product
     * @param that {Vector}
     * @return {Vector}
     */
    mulv(that) {
        return new Vector(this.a * that.a, this.b * that.b)
    }

    /**
     * @param that {Vector}
     * @return number
     */
    dot(that) {
        return this.a * that.a + this.b * that.b
    }

    /**
     * @return {Vector}
     */
    clone() {
        return new Vector(this.a, this.b)
    }

    /**
     * @return {number}
     */
    get length() {
        return Math.sqrt((this.a * this.a) + (this.b * this.b))
    }

    /**
     * Scales the vector so that it's length is exactly 1, but keeps the direction towards this vector is pointing.
     * This is the same as saying divide the vector by it's own length.
     * Special case, if the vector is the null vector (0, 0), then it returns another null vector.
     * @return {Vector} The vector with length 1, also known as a "unit" or "normal" vector and often written with a `^` (hat) like e.g. `û`
     */
    get normalize() {
        const length = this.length
        if(length === 0) {
            return Vector.null()
        } else {
            return new Vector(this.a / length, this.b / length)
        }
    }

    /**
     * Retrieves the angle of this vector to the x-axis in positive radians ranging from [0..2π].
     * If this is the null vector (0, 0), then the angle is zero.
     * @return {number}
     */
    get angle() {
        const length = this.length
        if(length === 0) {
            return 0
        }

        const angle_to_x_axis = Math.acos(this.a / length)
        const angle_to_y_axis = Math.asin(this.b / length)

        if(angle_to_y_axis < 0) {
            return -angle_to_x_axis + Math.PI * 2
        } else {
            return angle_to_x_axis
        }
    }

    /**
     * @return {string}
     */
    toString() {
        return `(${format(this.a) },${format(this.b)})`
    }

    get x() {
        return this.a
    }

    get y() {
        return this.b
    }
}
