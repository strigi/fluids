import {Rectangle} from './rectangle.js'
import {Vector} from './vector.js'

/**
 * @property x {number}
 * @property y {number}
 * @property field {Field}
 */
export class Cell {
    /**
     * @param x {number}
     * @param y {number}
     * @param field {Field}
     */
    constructor(x, y, field) {
        this.x = x
        this.y = y
        this.field = field
    }

    /**
     * @return {number}
     */
    get index() {
        return this.field.index(this.x, this.y)
    }

    /**
     * @param property_name {string} The name of the property to get in this cell. The property must be defined ahead of time on the cell's field.
     * @return {number} The value for the given property at this cell in the field.
     */
    get(property_name) {
        return this.field.property(property_name).get(this.x, this.y)
    }

    /**
     * TODO: extend this to objects of any depth? with arg constructor
     * @param property_name
     */
    get_vec(property_name) {
        return new Vector(
            this.field.property(`${property_name}.a`).get(this.x, this.y),
            this.field.property(`${property_name}.b`).get(this.x, this.y)
        )
    }

    /**
     * @template T
     * @param name {string}
     * @param value {T}
     */
    set(name, value) {
        this.field.property(name).set(this.x, this.y, value)
    }

    set_vec(property_name, value) {
        this.field.property(`${property_name}.a`).set(this.x, this.y, value.a)
        this.field.property(`${property_name}.b`).set(this.x, this.y, value.b)
    }

    /**
     * @return {Rectangle}
     */
    get bounds() {
        const field_bounds = this.field.bounds

        const top = field_bounds.top - this.field.spacing.height * (this.field.resolution.height - this.y - 1)
        const right = field_bounds.right - this.field.spacing.width * (this.field.resolution.width - this.x - 1)
        const bottom = field_bounds.bottom + this.field.spacing.height * this.y
        const left = field_bounds.left + this.field.spacing.width * this.x;

        return new Rectangle(top, right, bottom, left)
    }

    /**
     * @return {Vector}
     */
    get center() {
        return this.bounds.center
    }

    /**
     * @return {import('./size.js').Size}
     */
    get size() {
        return this.field.spacing;
    }
}
