import {Range} from './range.js'
import {validator, Validator} from '../utils/validator.js'

/**
 * @property name {string}
 * @property field {Field}
 * @property cells {Float32Array}
 */
export class Property {
    /**
     * @param name {string}
     * @param field {Field}
     * @param initial {number || function(): number}
     */
    constructor(name, field, initial= 0) {
        this.name = validator(name, 'name').string().assert()
        this.field = field
        this.initial = initial;
        this.cells = new Float32Array(this.field.count)

        if(!initial) {
            this.reset()
        }
    }

    /**
     * Sets (replaces) the value of this property at a specific cell in the field.
     * @param x {number} width index of the cell to set the value at. Zero based, bottom left origin.
     * @param y {number} height index of the cell to set the value at. Zero based, bottom left origin.
     * @param value {number} The value to write to the cell.
     */
    set(x, y, value) {
        this.cells[this.field.index(x, y)] = value
    }

    /**
     * Retrieves the value of this property at a specific cell in the field.
     * @param x {number} width index of the cell to retrieve the value from. Zero based, bottom left origin.
     * @param y {number} height index of the cell to retrieve the value from. Zero based, bottom left origin.
     * @return {number} The value at the cell indicated by `x` and `y`.
     */
    get(x, y) {
        return this.cells[this.field.index(x, y)]
    }

    /**
     * Adds the term value to the value at the cell and returns the sum.
     * Functionally the same as `set(x, y, get(x, y) + term)` but more convenient and more efficient.
     * @param x {number} width index of the cell to add the term to. Zero based, bottom left origin.
     * @param y {number} height index of the cell to add the term to. Zero based, bottom left origin.
     * @param term {number} the value to be added.
     * @return the new sum of the two values, which is also the value written to the cell.
     */
    add(x, y, term) {
        const index = this.field.index(x, y)
        this.cells[index] += term
        return this.cells[index]
    }

    /**
     * Resets all cells of this field back to its original initial value.
     */
    reset() {
        for(let i = 0; i < this.cells.length; i += 1) {
            this.cells[i] = typeof this.initial === 'function' ? this.initial() : this.initial
        }
    }

    /**
     * Return the difference between the largest and the smallest values of the entire field.
     * @return {Range}
     */
    range() {
        const accumulated = this.cells.reduce((accumulator, current) => {
            return [Math.min(accumulator[0], current), Math.max(accumulator[1], current)]
        }, [Number.MAX_VALUE, Number.MIN_VALUE])
        return new Range(accumulated[0], accumulated[1])
    }

    /**
     * Spawns a new property instance that is initialized to the parent property's initial value.
     * In other words, it creates a new empty clone. Any field values are not copied to the brood.
     * @return {Property}
     */
    spawn() {
        return new Property(this.name, this.field, this.initial)
    }
}
