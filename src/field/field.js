import {Size} from './size.js'
import {Property} from './property.js'
import {Cell} from './cell.js'
import {Rectangle} from './rectangle.js'

/**
 * @property size {Size}
 * @property resolution {Size}
 * @property properties {Map<string, Property>}
 */
export class Field {
    /**
     * @param size {Size}
     * @param resolution {Size}
     */
    constructor(size, resolution) {
        this.size = size
        this.resolution = resolution
        this.properties = new Map()
    }

    /**
     * @param property_name {string}
     * @param property_name {string}
     * @param [initial] {number | function(): number}
     * @return {Property}
     */
    define(property_name, initial) {
        const property = new Property(property_name, this, initial)
        this.properties.set(property_name, property)
        return property
    }

    define_vec(property_name, initial) {
        return [
            this.define(`${property_name}.a`, initial.a),
            this.define(`${property_name}.b`, initial.b)
        ]
    }

    get half_size() {
        return this.size.div(2)
    }

    /**
     * @return {Size}
     */
    get spacing() {
        return new Size(this.size.width / this.resolution.width, this.size.height / this.resolution.height)
    }

    /**
     * @return {Rectangle}
     */
    get bounds() {
        const half_size = this.half_size
        return new Rectangle(half_size.height, half_size.width, -half_size.height, -half_size.width)
    }

    /**
     * @param x {number}
     * @param y {number}
     * @return {number}
     */
    index(x, y) {
        if(x >= this.resolution.width || x < 0 || y >= this.resolution.height || y < 0) {
            throw new Error(`Index (x=${x}, y=${y}) out of bounds [x=0..${this.resolution.width - 1}, y=0..${this.resolution.height - 1}]`)
        }

        return x + y * this.resolution.width
    }

    cell(x, y) {
        return new Cell(x, y, this)
    }

    /**
     * @return {number}
     */
    get count() {
        return this.resolution.area;
    }

    /**
     * @param fn {function(cell: Cell, x: number, y: number, index: number, field: Field)}
     * @deprecated Use {@link cells()} instead
     */
    each(fn) {
        for(let y = 0; y < this.resolution.height; y++) {
            for(let x = 0; x < this.resolution.width; x++) {
                const cell = this.cell(x, y)
                fn(cell, x, y, this.index(x, y), this)
            }
        }
    }

    *cells() {
        for(let y = 0; y < this.resolution.height; y++) {
            for(let x = 0; x < this.resolution.width; x++) {
                yield this.cell(x, y)
            }
        }
    }

    /**
     * @param name {string}
     * @return {Property}
     */
    property(name) {
        if(!this.properties.has(name)) {
            throw new Error(`Field does not have property named '${name}', only [${[...this.properties.keys()].join()}]`)
        }
        return this.properties.get(name)
    }

    tick() {
        const current = [this.property('velocity.a'), this.property('velocity.b')]
        const next = [current[0].spawn(), current[1].spawn()]

        const half_size = this.half_size

        for(const cell of this.cells()) {
            const from = cell.center
            const by = cell.get_vec('velocity')
            if(by.length === 0) {
                console.debug("Zero length velocity skipped")
                continue;
            }

            const to = from.add(by)

            const x = (to.x + half_size.width) / this.spacing.width
            let x1 = Math.floor(x)
            let x2 = x - x1 < 0.5 ? x1 - 1 : x1 + 1
            if (x1 > x2) {
                let tmp = x1
                x1 = x2
                x2 = tmp
            }

            const y = (to.y + half_size.height) / this.spacing.height
            let y1 = Math.floor(y)
            let y2 = y - y1 < 0.5 ? y1 - 1 : y1 + 1
            if (y1 > y2) {
                let tmp = y1
                y1 = y2
                y2 = tmp
            }

            if(x1 < 0 || x1 >= this.resolution.width || x2 < 0 || x2 >= this.resolution.width || y1 < 0 || y1 >= this.resolution.height || y2 < 0 || y2 >= this.resolution.height) {
                continue
            }

            const top_right = this.cell(x2, y2)
            const top_left = this.cell(x1, y2)
            const bottom_left = this.cell(x1, y1)
            const bottom_right = this.cell(x2, y1)
            const receiver_cells = [top_right, top_left, bottom_left, bottom_right]


            const top_right_delta = top_right.center.sub(to)
            const top_left_delta = top_left.center.sub(to)
            const bottom_left_delta = bottom_left.center.sub(to)
            const bottom_right_delta = bottom_right.center.sub(to)

            const sum_length = 1 / top_right_delta.length + 1 / top_left_delta.length + 1 / bottom_left_delta.length + 1 / bottom_right_delta.length
            const top_right_fraction = 1 / top_right_delta.length / sum_length
            const top_left_fraction = 1 / top_left_delta.length / sum_length
            const bottom_left_fraction = 1 / bottom_left_delta.length / sum_length
            const bottom_right_fraction = 1 / bottom_right_delta.length / sum_length

            const top_right_result = by.muls(top_right_fraction)
            const top_left_result = by.muls(top_left_fraction)
            const bottom_left_result = by.muls(bottom_left_fraction)
            const bottom_right_result = by.muls(bottom_right_fraction)

            const result_velocities = [top_right_result, top_left_result, bottom_left_result, bottom_right_result]

            for (const i in result_velocities) {
                const recipient = receiver_cells[i]
                next[0].add(recipient.x, recipient.y, result_velocities[i].x)
                next[1].add(recipient.x, recipient.y, result_velocities[i].y)
            }
        }

        this.properties.set(next[0].name, next[0])
        this.properties.set(next[1].name, next[1])
    }
}
