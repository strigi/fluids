import {Size} from './size.js'
import {Vector} from './vector.js'

/**
 * @property top {number}
 * @property right {number}
 * @property bottom {number}
 * @property left {number}
 */
export class Rectangle {
    /**
     *
     * @param top {number}
     * @param right {number}
     * @param bottom {number}
     * @param left {number}
     */
    constructor(top, right, bottom, left) {
        this.top = top
        this.right = right
        this.bottom = bottom
        this.left = left
    }

    /**
     * @return {Size}
     */
    get size() {
        const width = this.right - this.left
        const height = this.top - this.bottom
        return new Size(width, height)
    }

    /**
     * @return {Vector}
     */
    get center() {
        const half_size = this.size.div(2)
        return new Vector(this.left + half_size.width, this.bottom + half_size.height)
    }
}
