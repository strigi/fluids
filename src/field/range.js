export class Range {
    /**
     * @param a {number}
     * @param b {number}
     */
    constructor(a, b) {
        if(a > b) {
            this.min = b
            this.max = a
        } else {
            this.min = a
            this.max = b
        }
    }

    /**
     * @return {number}
     */
    get size() {
        return this.max - this.min
    }

    toString(fractionDigits = 2) {
        return `${this.min.toFixed(fractionDigits)}..${this.max.toFixed(fractionDigits)}`
    }
}
