export class Size {
    /**
     * @param width {number}
     * @param height {number}
     */
    constructor(width, height) {
        this.width = width
        this.height = height
    }

    /**
     * @return {number}
     */
    get area() {
        return this.width * this.height
    }

    /**
     * @param scalar {number}
     */
    div_assign(scalar) {
        this.width /= scalar
        this.height /= scalar
    }

    /**
     * @param scalar {number}
     * @return {Size}
     */
    div(scalar) {
        return new Size(this.width / scalar, this.height / scalar);
    }

    toString(fractionDigits = 2) {
        return `${this.width.toFixed(fractionDigits)}x${this.height.toFixed(fractionDigits)}`
    }
}
