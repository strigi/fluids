/**
 * @param radians {number}
 * @return {number}
 */
export function to_deg(radians) {
    return 180 * radians / Math.PI
}

/**
 * @param degrees {number}
 * @return {number}
 */
export function to_rad(degrees) {
    return Math.PI * degrees / 180
}

export function lerp(a, b, ratio) {
    return a * (1 - ratio) + b * ratio
}
