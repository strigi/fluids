import {createApp} from 'vue'

import './style.scss'
import root from './root.vue'

createApp(root).mount('#app')
